var DMAIL = {
    initialize: function () {
        DMAIL.loadUserInfo();
        DMAIL.leerFromLocalStorage();
    },
   
    leerFromLocalStorage: function (){

        var correo =[];
        correo= JSON.parse( localStorage.getItem('leer'));
        
  

        
        document.getElementById("email").value = correo.email;   
        document.getElementById("asunto").value = correo.asunto;
        document.getElementById("mensaje").value = correo.mensaje;
     

     

    },
  
    loadUserInfo: function () {
    
    if (localStorage.getItem('islogin')) {
        var user = [];
        user = JSON.parse(localStorage.getItem('islogin'));
        if (document.getElementById('settings')) {
            document.getElementById('full_name').value = user.firstName + " " + user.lastName;
            document.getElementById('speed').value = user.speed;
            document.getElementById('aboutme').value = user.aboutme;
        }

        if (document.getElementById('userName')) {
            var username = document.getElementById('userName');
            
            username.innerHTML = user.userName + username.innerHTML;
        }
        return user.userName;
    }
    },

    
    logout: function () {
        if (localStorage.getItem('islogin')) {
            localStorage.removeItem('islogin');
            location.href = "login.html";
        }
    } 


};
DMAIL.initialize();
