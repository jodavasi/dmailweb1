var DMAIL = {
    initialize: function () { 
        
        if (document.getElementById('myTableData2')) {
            DMAIL.loadUserMail();
        }
       DMAIL.loadUserInfo();
    },
    

    loadUserMail: function () {
        if (localStorage.getItem('islogin')) {
            if (document.getElementById('myTableData2')) {
                var borradores = [];

                DMAIL.clearTable();

                if (localStorage.getItem('borradores')) {
                    borradores = JSON.parse(localStorage.getItem('borradores'));
                }

                var table = document.getElementById("myTableData2");
                var user = JSON.parse(localStorage.getItem('islogin'));
             
                borradores.reverse().forEach(function (borrador, index) {
                    if (borrador.userName === user.userName) {
                       
                       
                        var tr = document.createElement("tr"),
                        tdFrom = document.createElement("td"),
                        tdBy = document.createElement("td"),
                        tdSubject = document.createElement("td"),
                        tdRemove = document.createElement("td"),
                        btnEdit = document.createElement("button");
                        btnRemove = document.createElement("button");

                    tdFrom.innerHTML = borrador.email;
                    tdBy.innerHTML = borrador.asunto;
                    tdSubject.innerHTML = borrador.mensaje;

                    btnRemove.textContent = 'Delete';
                    btnRemove.id='bot';
                    btnRemove.className = 'btn btn-xs btn-danger';
                    btnRemove.addEventListener('click', function(){
                        // alert(borradores);
                        // alert(index);
                        removeFromLocalStorage(index);
                    });

                    btnEdit.textContent = 'Edit';
                    btnEdit.id='le';
                    btnEdit.className = 'btn     btn-xs btn-danger';
                    btnEdit.addEventListener('click', function(){
                        leerFromLocalStorage(index);
                        location.href = "index_editar.html";
                        removeFromLocalStorage(index);
                    });
                    tdRemove.appendChild(btnEdit);
                    tdRemove.appendChild(btnRemove);

                    tr.appendChild(tdFrom);
                    tr.appendChild(tdBy);
                    tr.appendChild(tdSubject);
                    tr.appendChild(tdRemove);

                    table.appendChild(tr);
                    }
                });
                 function removeFromLocalStorage(index){
                     
                    var borradores = [];
                    
                    dataInLocalStorage = localStorage.getItem('borradores');
                    
                    borradores = JSON.parse(dataInLocalStorage);
            
                    borradores.splice(index, 1);
            
                    localStorage.setItem('borradores', JSON.stringify(borradores));
            
                    DMAIL.loadUserMail();
                }

                function leerFromLocalStorage(index){
                    
                    var borradores = [];
                    
                    dataInLocalStorage = localStorage.getItem('borradores');
                    
                    borradores = JSON.parse(dataInLocalStorage);
                    
                    localStorage.setItem('position', index);

                    
                    borradores.forEach(function (borrador, index2) {
                        
                      if(index2===index){
                          
                          localStorage.setItem('edit', JSON.stringify(borrador));
                          
                    }
                    });

                    // borradores.splice(index, 1);
            
                    // localStorage.setItem('borradores', JSON.stringify(borradores));
            
                    // DMAIL.loadUserMail();
                }
            
            }
        }if (document.getElementById('logout')) {
            document.getElementById('logout').addEventListener('click', function () {
                DMAIL.logout();
            });
            
        }
    },
    loadUserInfo: function () {
        
        if (localStorage.getItem('islogin')) {
            var user = [];
            user = JSON.parse(localStorage.getItem('islogin'));
            if (document.getElementById('settings')) {
                document.getElementById('full_name').value = user.firstName + " " + user.lastName;
                document.getElementById('speed').value = user.speed;
                document.getElementById('aboutme').value = user.aboutme;
            }

            if (document.getElementById('userName')) {
                var username = document.getElementById('userName');
                var usernameside = document.getElementById('userNameSide');
                username.innerHTML = user.userName + username.innerHTML;
                usernameside.innerHTML = usernameside.innerHTML + user.userName;
            }

            return user.userName;
        }
    },
    logout: function () {
        if (localStorage.getItem('islogin')) {
            localStorage.removeItem('islogin');
            location.href = "login.html";
        }
    },
    clearTable: function (table) {
        if (document.getElementById('myTableData')) {
            document.getElementById("myTableData").innerHTML = "";
        }
        if (document.getElementById('myTableData2')) {
            document.getElementById("myTableData2").innerHTML = "";
        }

    }

};
DMAIL.initialize();
