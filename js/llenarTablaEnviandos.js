var DMAIL = {
    initialize: function () { 
        
        if (document.getElementById('myTableData1')) {
            DMAIL.loadUserMail();
        }
       DMAIL.loadUserInfo();
    },
    

    loadUserMail: function () {
        if (localStorage.getItem('islogin')) {
            if (document.getElementById('myTableData1')) {
                var correos = [];

                DMAIL.clearTable();

                if (localStorage.getItem('correos')) {
                    correos = JSON.parse(localStorage.getItem('correos'));
                }

                var table = document.getElementById("myTableData1");
                var user = JSON.parse(localStorage.getItem('islogin'));
             
                correos.reverse().forEach(function (correo, index) {
                    if (correo.userName === user.userName) {
                       
                       
                        var tr = document.createElement("tr"),
                        tdFrom = document.createElement("td"),
                        tdBy = document.createElement("td"),
                        tdSubject = document.createElement("td"),
                        tdRemove = document.createElement("td"),
                        btnLeer = document.createElement("button");
                        btnRemove = document.createElement("button");

                    tdFrom.innerHTML = correo.email;
                    tdBy.innerHTML = correo.asunto;
                    tdSubject.innerHTML = correo.mensaje;

                    btnRemove.textContent = 'Delete';
                    btnRemove.id='bot';
                    btnRemove.className = 'btn btn-xs btn-danger';
                    btnRemove.addEventListener('click', function(){
                        // alert(correos);
                        // alert(index);
                        removeFromLocalStorage(index);
                    });

                    btnLeer.textContent = 'Leer';
                    btnLeer.id='le';
                    btnLeer.className = 'btn btn-xs btn-danger';
                    btnLeer.addEventListener('click', function(){
                        leerFromLocalStorage(index);
                        location.href = "index_leer.html";
                    });
                    tdRemove.appendChild(btnLeer);
                    tdRemove.appendChild(btnRemove);

                    tr.appendChild(tdFrom);
                    tr.appendChild(tdBy);
                    tr.appendChild(tdSubject);
                    tr.appendChild(tdRemove);

                    table.appendChild(tr);
                    }
                });
                 function removeFromLocalStorage(index){
                     
                    var correos = [];
                    
                    dataInLocalStorage = localStorage.getItem('correos');
                    
                    correos = JSON.parse(dataInLocalStorage);
            
                    correos.splice(index, 1);
            
                    localStorage.setItem('correos', JSON.stringify(correos));
            
                    DMAIL.loadUserMail();
                }

                function leerFromLocalStorage(index){
                    
                    var correos = [];
                    
                    dataInLocalStorage = localStorage.getItem('correos');
                    
                    correos = JSON.parse(dataInLocalStorage);
                    
                    localStorage.setItem('position', index);

                    
                    correos.forEach(function (correo, index2) {
                        
                      if(index2===index){
                          
                          localStorage.setItem('leer', JSON.stringify(correo));
                          
                    }
                    });

                    // correos.splice(index, 1);
            
                    // localStorage.setItem('correos', JSON.stringify(correos));
            
                    // DMAIL.loadUserMail();
                }
            
            }
        }if (document.getElementById('logout')) {
            document.getElementById('logout').addEventListener('click', function () {
                DMAIL.logout();
            });
            
        }
    },
    loadUserInfo: function () {
        
        if (localStorage.getItem('islogin')) {
            var user = [];
            user = JSON.parse(localStorage.getItem('islogin'));
            if (document.getElementById('settings')) {
                document.getElementById('full_name').value = user.firstName + " " + user.lastName;
                document.getElementById('speed').value = user.speed;
                document.getElementById('aboutme').value = user.aboutme;
            }

            if (document.getElementById('userName')) {
                var username = document.getElementById('userName');
                var usernameside = document.getElementById('userNameSide');
                username.innerHTML = user.userName + username.innerHTML;
                usernameside.innerHTML = usernameside.innerHTML + user.userName;
            }

            return user.userName;
        }
    },
    logout: function () {
        if (localStorage.getItem('islogin')) {
            localStorage.removeItem('islogin');
            location.href = "login.html";
        }
    },
    clearTable: function (table) {
        if (document.getElementById('myTableData')) {
            document.getElementById("myTableData").innerHTML = "";
        }
        if (document.getElementById('myTableData1')) {
            document.getElementById("myTableData1").innerHTML = "";
        }

    }

};
DMAIL.initialize();
