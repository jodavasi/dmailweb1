//document.getElementById("login").addEventListener("click", validar);

var DMAIL = {
    initializeEvents: function () {
       
if (document.getElementById('login')) {
    document.getElementById('login').addEventListener('click', function () {
        // obtener la información del form
        DMAIL.Login();
    });
}
    },
    Login: function () {   
    if (document.getElementById('username') && document.getElementById('pass')) {
        
        var users = [];
        if (localStorage.getItem('users')) {
            users = JSON.parse(localStorage.getItem('users'));

            users.forEach(function (user, index, users) {
                
                if (user.userName === document.getElementById('username').value && 
                user.password === document.getElementById('pass').value) {
                   
                    localStorage.setItem('islogin', JSON.stringify(user));
                    setTimeout(function(){
                        location.href = "index_recibidos.html";  
                    },1);
                }
                
            });
        }
    }
    }
};
DMAIL.initializeEvents();
