






var botonEnviar = document.getElementById('btnEnviar').addEventListener('click', registrarCorreo);
var botonCancelar = document.getElementById('btnCancelar').addEventListener('click', cancelarCorreo);

function registrarCorreo(){


    var correo = {
        userName: loadUserInfo(),
        email: document.getElementById('email').value,
        asunto: document.getElementById('asunto').value,
        mensaje: document.getElementById('mensaje').value,
    };
    
    var mensajeEnv = [];
    if (localStorage.getItem('correos')) {
        mensajeEnv = JSON.parse(localStorage.getItem('correos'));
    }
    mensajeEnv.push(correo);

    localStorage.setItem('correos', JSON.stringify(mensajeEnv));
  
    console.log(mensajeEnv.innerHTML);
}
function cancelarCorreo(){

    var borrador = {
        userName: loadUserInfo(),
        email: document.getElementById('email').value,
        asunto: document.getElementById('asunto').value,
        mensaje: document.getElementById('mensaje').value,
    };
    
    var mensajeBor = [];
    if (localStorage.getItem('borradores')) {
        mensajeBor = JSON.parse(localStorage.getItem('borradores'));
    }
    mensajeBor.push(borrador);

    localStorage.setItem('borradores', JSON.stringify(mensajeBor));
  
    console.log(mensajeBor.innerHTML);

    
}

function loadUserInfo() {
    
    if (localStorage.getItem('islogin')) {
        var user = [];
        user = JSON.parse(localStorage.getItem('islogin'));
        if (document.getElementById('settings')) {
            document.getElementById('full_name').value = user.firstName + " " + user.lastName;
            document.getElementById('speed').value = user.speed;
            document.getElementById('aboutme').value = user.aboutme;
        }

        if (document.getElementById('userName')) {
            var username = document.getElementById('userName');
            
            username.innerHTML = user.userName + username.innerHTML;
        }
        return user.userName;
    }
    }





